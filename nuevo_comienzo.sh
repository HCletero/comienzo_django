#Este script tiene la intención de crear las carpetas bases para generar un proyecto, una vez que se realize 
#esta operación se puede crear los apps-individuales

#para darle permisos es necesario escribir: "chmod a+x nuevo_comienzo.sh
#from .creacion_carpetas

#from .modificar_archivos
#!/bin/sh

echo "-----------------------"
read -p '¿Como se llamará el proyecto?: ' proyecto
echo "El proyecto se llama: " $proyecto
echo "-----------------------"

#read -p '¿creaste un ambiente virtual? (si/no) ' respuesta_creacion

#if ["$respuesta_creacion" == 'si']
#then
#	read -p '¿inicializaste el ambiente? (si/no) ' respuesta_inicializacion
#	
#    if ["$respuesta_inicializacion" == 'no']
#    then
#        read -p 'escriba el nombre del ambiente virtual para entrar en el: ' ambiente
#        workon $ambiente;
#    elif ["$respuesta_inicialización" == 'si']
		read -p 'escriba el nombre de su ambiente virtual: ' ambiente
#		echo "ya se encuentra en "$ambiente;
#    else
#        echo "Error, debe responder si o no, comienze nuevamente"
#        exit;
#	fi
#elif ["$respuesta_creacion" == 'no' ]
#    mkvirtualenv $proyecto;
#else
#    echo "Error, debe responder si o no, comienze nuevamente"
#    exit;
#	fi

#	post='~/.virtualenvs/${ambiente}/bin/' #postactivate"
#	pre="~/.virtualenvs/${ambiente}/bin/" #preactivate"
#	ls ${post}
	


#utilizar después de crear el ambiente virtual;
#para crear ambiente virtual: "mkvirtualenv nom_ambiente"

cd ..
echo "instalando django"
pip install django
echo "iniciando el proyecto"
django-admin.py startproject $proyecto

echo "--------------------"
echo "RECUERDA INICIAR REPOSITORIO EN GIT"
echo "--------------------"

#carpetas()
#archivos()

#	projecto=$(pwd|awk -F'/' '{print $NF}')

#function carpetas(proyecto ) {
	cd ./$proyecto
	echo "creando carpetas para este nuevo proyecto web"
	mkdir ./apps
	mkdir ./files
	mkdir ./files/fixtures
	mkdir ./files/global_static
	mkdir ./files/global_static/css
	mkdir ./files/global_static/framework
	mkdir ./files/media
	mkdir ./files/statics
	mkdir ./files/templates



	#mkdir ./$proyecto
	mkdir ./$proyecto/settings

	echo "creadas las carpetas"
	echo "cambiando de nombre a settings"

	mv ./$proyecto/settings.py ./$proyecto/settings/base.py

#}

#function archivos(ambiente, proyecto){
	cd ..
	echo "$proyecto"
	base="./${proyecto}/${proyecto}/settings/base.py"
	dev="./${proyecto}/${proyecto}/settings/dev.py"
	prod="./${proyecto}/${proyecto}/settings/prod.py"
	manage="./${proyecto}/manage.py"
#	post="~/.virtualenvs/${ambiente}/bin/postactivate"
#	pre="~/.virtualenvs/${ambiente}/bin/preactivate"

	echo "modificando base.py"
	sed -i "/import/i\from pathlib import Path" $base 	
	echo "fase2"
	sed -i "/import/i\SETTINGS_PATH = Path(__file__)" $base 
	echo "fase3" $base
	sed -i 's/BASE_DIR = .*/BASE_DIR = str(SETTINGS_PATH.resolve().parent.parent.parent)/g' $base
	

	echo "Creando y modificando dev.py y prod.py"

	
	echo "from .base import *

DEBUG = True

DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.sqlite3',
			'NAME': os.path.join(BASE_DIR, 'db.sqlite3').
		}
	}
SITE_URL=\"http://www.escuelapermacultura.cl\"
SITE_NAME=\"Escuela Cooperativa de Permacultura\"
GOOGLE_API_KEY=''
SITE_ID = 1
ALLOWED_HOSTS=[\"*\"]"  >> $dev

	echo "from .base import *
DEBUG = False
# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DBNAME = 'misdatos'
DBUSER = 'usuario'
DBPASS = 'mipassword'
engine='django.db.backends.postgresql_psycopg2'
DATABASES = {
	'default': {
		'ENGINE': engine,
		'NAME': DBNAME,
		'USER': DBUSER,
		'PASSWORD':DBPASS,
		'HOST': '',
		'PORT': '',
	},
}
SITE_URL=\"http://www.miweb.cl\"
SITE_NAME=\"Mi Web\"
ALLOWED_HOSTS=['localhost',
				'miweb.cl',]
SITE_ID = 1 ">> $prod

	
	echo "modificando manage.py"

	sed -i "s/os.environ.setdefault('DJANGO_SETTINGS_MODULE', '"$proyecto".settings/os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mi_web.settings.dev/g" $manage|
	
	echo "protegiendo la SECRET_KEY"
	
	cat $base|grep 'SECRET_KEY =.*' > secret_key_$proyecto
	sed -n 's/SECRET_KEY = .*/SECRET_KEY = get_env_variable('SECRET_KEY')/g' $base
	
	key=$(cat secret_key_$proyecto)
	
	cd ~/.virtualenvs/${ambiente}/bin/
	echo "export "$key"
export STAGE=\'dev\'" > postactivate
	
	echo "unset SECRET_KEY
unset DJANGO_STAGE" > preactivate
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
#}













