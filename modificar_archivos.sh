function archivos(ambiente, proyecto){
	
	base = './$proyecto/$proyecto/settings/base.py'
	dev = './$proyecto/$proyecto/settings/dev.py'
	prod = './$proyecto/$proyecto/settings/prod.py'
	manage = './$proyecto/manage.py'
	post = '~/.virtualenvs/$ambiente/bin/postactivate'
	pre = '~/.virtualenvs/$ambiente/bin/preactivate'

	sed -i "/import/i\from pathlib import Path" $base 	
	sed -i "/import/i\SETTINGS_PATH = Path(__file__)" $base 
	cat ./$base|sed -i 's/BASE_DIR = os.*/BASE_DIR = str(SETTINGS_PATH.resolve().parent.parent.parent)/g'


	echo "Creando y modificando dev.py y prod.py"
	touch dev.py prod.py
	
	echo "from .base import *

DEBUG = True

DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.sqlite3',
			'NAME': os.path.join(BASE_DIR, 'db.sqlite3').
		}
	}
SITE_URL=\"http://www.escuelapermacultura.cl\"
SITE_NAME=\"Escuela Cooperativa de Permacultura\"
GOOGLE_API_KEY=''
SITE_ID = 1
ALLOWED_HOSTS=[\"*\"]"  >> $dev

	echo "from .base import *
DEBUG = False
# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DBNAME = 'misdatos'
DBUSER = 'usuario'
DBPASS = 'mipassword'
engine='django.db.backends.postgresql_psycopg2'
DATABASES = {
	'default': {
		'ENGINE': engine,
		'NAME': DBNAME,
		'USER': DBUSER,
		'PASSWORD':DBPASS,
		'HOST': '',
		'PORT': '',
	},
}
SITE_URL=\"http://www.miweb.cl\"
SITE_NAME=\"Mi Web\"
ALLOWED_HOSTS=['localhost',
				'miweb.cl',]
SITE_ID = 1 ">> $prod

	
	echo "modificando manage.py"

	cat $manage|sed -i "s/os.environ.setdefault('DJANGO_SETTINGS_MODULE', '"$proyecto".settings/os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mi_web.settings.dev/g"
	
	echo "protegiendo la SECRET_KEY"
	
	cat $base|grep 'SECRET_KEY =\.' > secret_key
	cat $base|sed 's/SECRET_KEY =\./SECRET_KEY = get_env_variable('SECRET_KEY')/g'
	
	key=$(cat secret_key)
	echo "export "$key"
	export STAGE=\'dev\'" >> $post
	
	echo "unset SECRET_KEY
	unset DJANGO_STAGE" >> $pre
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}








